import $ from 'jquery';

export default function() {
  
        $(".gr-dropzone").each(function () {

            const $this = $(this);
            const zoneId = $this.data("gr-zone");
   
            if (zoneId) {
                const $zone = $("#" + zoneId);
                if ($zone.length > 0) {
                  
                    $zone.html($this.html());   // copy the html to where it should be
                } else {

                }
            } else {

            }
            $this.remove();			// remove the html from its temporary location
        });
    
    
}
